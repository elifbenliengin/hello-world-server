#!/bin/sh

docker login -u $DOCKER_USER -p $DOCKER_PASSWORD
docker build -t benliel/hello-world:latest .
docker push benliel/hello-world:latest
